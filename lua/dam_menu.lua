DisableAllMods                  = DisableAllMods or {}
DisableAllMods._loc_path        = ModPath .. 'loc/'
DisableAllMods._save_path       = SavePath .. 'disable_all_mods.txt'
DisableAllMods._exclusions_path = SavePath .. 'blt_exclusions.txt'
DisableAllMods._settings        = {
  mods_state = {},  -- currently enabled and disabled mods
  exclusions = {}   -- mods that we don't enable or disable
}

--#region Load / Save Settings
function DisableAllMods.file_info()
  local path = {DisableAllMods._save_path, DisableAllMods._exclusions_path}
  local settings = {
    DisableAllMods._settings.mods_state,
    DisableAllMods._settings.exclusions
  }

  return path, settings
end

function DisableAllMods.load()
  local path, settings = DisableAllMods.file_info()

  for i in ipairs(path) do
    local file = io.open(path[i])

    if file then
      local data = json.decode(file:read())

      for k, v in pairs(data) do
        settings[i][k] = v
      end

      file:close()
    elseif path[i] == DisableAllMods._exclusions_path then
      -- Generate exclusions template
      DisableAllMods._settings.exclusions["Alphasort Mods"]                 = true
      DisableAllMods._settings.exclusions["Extra Profiles and Skill Sets"]  = true

      DisableAllMods.save()
    end
  end
end

function DisableAllMods.save()
  local path, settings = DisableAllMods.file_info()

  for i in ipairs(path) do
    local file = io.open(path[i], "w")

    if file then
      local data = json.encode(settings[i])

      file:write(data)
      file:close()
    end
  end
end
--#endregion

--#region Localization
function DisableAllMods.get_loc_file()
  local lang = BLT.Localization:get_language().language

  if lang then
    local file = DisableAllMods._loc_path .. lang .. '.txt'

    if io.file_is_readable(file) then
      return file
    end
  end

  return DisableAllMods._loc_path .. 'en.txt'
end

function DisableAllMods.init_loc()
  Hooks:AddHook("LocalizationManagerPostInit", "disableallmods_loc_postinit_id",
    function (self)
      local loc_file = DisableAllMods.get_loc_file()

      self:load_localization_file(loc_file, false)
    end
  )
end
--#endregion

function DisableAllMods.init()
  DisableAllMods.load()
  DisableAllMods.init_loc()
end

DisableAllMods.init()