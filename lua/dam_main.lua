dofile(ModPath .. "lua/dam_menu.lua")

DisableAllMods            = DisableAllMods or {}
DisableAllMods._blt_path  = SavePath .. 'blt_data.txt'

function DisableAllMods.get_mods_file()
  local file = io.open(DisableAllMods._blt_path, "r")

  if file then
    local data = json.decode(file:read())

    file:close()

    if data and data["mods"] then
      local mods_table = data["mods"]

      return true, data, mods_table
    end
  end

  return false
end

--#region First Run
function DisableAllMods.first_run()
  local empty = next(DisableAllMods._settings.mods_state) == nil
  return empty
end
--#endregion

--#region Keybind Menu
function DisableAllMods.keybind_menu()
  local options = {}
  local first_run = DisableAllMods.first_run()
  local desc = first_run and DisableAllMods.text("disableallmods_first_run_desc") or nil

  options[#options + 1] = {
    text = DisableAllMods.text("disableallmods_save_state_btn"),
    callback = DisableAllMods.save_state
  }

  if not first_run then
    local a = DisableAllMods.text("disableallmods_menu_desc")
    local b = DisableAllMods.text("disableallmods_save_state_desc")
    local c = DisableAllMods.text("disableallmods_disable_mods_desc")
    local d = DisableAllMods.text("disableallmods_restore_mods_desc")

    desc =  a .. b .. c .. d

    options[#options + 1] = {
      text = DisableAllMods.text("disableallmods_disable_mods_btn"),
      callback = function () DisableAllMods.toggle(false) end
    }
    options[#options + 1] = {
      text = DisableAllMods.text("disableallmods_restore_mods_btn"),
      callback = function () DisableAllMods.toggle(true) end
    }
  end

  local menu = DisableAllMods.quick_menu(nil, desc, options, true)

  menu:Show()
end
--#endregion

--#region Save state of enabled / disabled mods
function DisableAllMods.save_state()
  local success, _, mods_table = DisableAllMods.get_mods_file("r")

  if success then

    if mods_table then
      local excluded   = DisableAllMods._settings.exclusions

      for k, _ in pairs(mods_table) do
        local is_valid        = type(mods_table[k].enabled) == "boolean"
        local is_not_core_mod = k ~= "base" and k ~= "Disable All Mods"
        local is_not_excluded = not excluded[k]

        if is_not_excluded and is_not_core_mod and is_valid then
          DisableAllMods._settings.mods_state[k] = mods_table[k].enabled
        end
      end

      DisableAllMods.save()
      DisableAllMods.save_state_success_menu()
    end
  end
end

function DisableAllMods.save_state_success_menu()
  local desc = DisableAllMods.text("disableallmods_save_state_complete_desc")
  local options = {}
  options[#options + 1] = {
    text = DisableAllMods.text("disableallmods_ok_text"),
    callback = DisableAllMods.keybind_menu
  }
  local menu = DisableAllMods.quick_menu(nil, desc, options)

  menu:Show()
end
--#endregion

--#region Toggle mods
function DisableAllMods.toggle(enabled)
  local success, data, _ = DisableAllMods.get_mods_file("r")

  if success then

    if data and data["mods"] then
      for k, v in pairs(data["mods"]) do
        local is_not_excluded = not DisableAllMods._settings.exclusions[k]
        local is_not_core_mod = k ~= "base" and k ~= "Disable All Mods"
        local is_valid        = DisableAllMods._settings.mods_state[k]

        if is_not_excluded and is_not_core_mod and is_valid then
          data.mods[k].enabled = enabled
        end
      end

      DisableAllMods.save_blt_data(data)
      DisableAllMods.confirm_restart_menu()
    end
  end
end
--#endregion

--#region Save BLT Data
function DisableAllMods.save_blt_data(data)
  if data then
    local file = io.open(DisableAllMods._blt_path, "w")

    if file then
      local d = json.encode(data)

      file:write(d)
      file:close()
    end
  end
end
--#endregion

--#region Restart Payday 2
function DisableAllMods.confirm_restart_menu()
  local desc    = DisableAllMods.text("disableallmods_restart_desc")
  local yes     = DisableAllMods.text("disableallmods_yes_text")
  local no      = DisableAllMods.text("disableallmods_no_text")
  local options = {}

  options[#options + 1] = {
    text      = yes,
    callback  = DisableAllMods.restart
  }

  local menu = DisableAllMods.quick_menu(nil, desc, options, true, no)

  menu:Show()
end

function DisableAllMods.restart()
  setup:load_start_menu()
end
--#endregion

--#region Utilities
function DisableAllMods.quick_menu(title , desc, options, is_cancel, cancel_text)
  title   = title or DisableAllMods.text("disableallmods_menu_title")
  desc    = desc or DisableAllMods.text("disableallmods_menu_desc")
  options = options or {}

  if is_cancel then
    cancel_text = cancel_text or DisableAllMods.text("disableallmods_cancel_text")
    options[#options + 1] = {
      text              = cancel_text,
      is_cancel_button  = true
    }
  end

  return QuickMenu:new(title,desc,options)
end

function DisableAllMods.text(loc_key)
  return managers.localization:text(loc_key)
end
--#endregion